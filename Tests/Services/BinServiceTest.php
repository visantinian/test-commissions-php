<?php

namespace Tests\Services;

use PHPUnit\Framework\TestCase;
use App\Services\BinService;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;

class BinServiceTest extends TestCase {
  public function testGetBinDetailsIsEu(): void {
    $mockClient = $this->createMock(ClientInterface::class);
    $mockResponse = new Response(200, [], json_encode([
      'country' => ['alpha2' => 'FR']
    ]));

    $mockClient->method('request')->willReturn($mockResponse);

    $binService = new BinService($mockClient);
    $binDetails = $binService->getBinDetails('45717360');
    $this->assertTrue($binDetails->isEu);
  }

  public function testGetBinDetailsIsNotEu(): void {
    $mockClient = $this->createMock(ClientInterface::class);
    $mockResponse = new Response(200, [], json_encode([
      'country' => ['alpha2' => 'US']
    ]));

    $mockClient->method('request')->willReturn($mockResponse);

    $binService = new BinService($mockClient);
    $binDetails = $binService->getBinDetails('516793');
    $this->assertFalse($binDetails->isEu);
  }
}
