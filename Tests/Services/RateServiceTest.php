<?php

namespace Tests\Services;

use PHPUnit\Framework\TestCase;
use App\Services\RateService;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;

class RateServiceTest extends TestCase {
  private RateService $rateService;
  private $mockClient;

  protected function setUp(): void {
    $this->mockClient = $this->createMock(ClientInterface::class);
    $this->rateService = new RateService($this->mockClient);
  }

  public function testGetRateForEUR(): void {
    $mockResponse = new Response(200, [], json_encode(['rates' => ['EUR' => 1.0]]));
    $this->mockClient->method('request')->willReturn($mockResponse);

    $rate = $this->rateService->getRate('EUR');
    $this->assertEquals(1.0, $rate);
  }

  public function testGetRateForUSD(): void {
    $mockResponse = new Response(200, [], json_encode(['rates' => ['USD' => 1.1]]));
    $this->mockClient->method('request')->willReturn($mockResponse);

    $rate = $this->rateService->getRate('USD');
    $this->assertEquals(1.1, $rate);
  }
}
