<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\TransactionProcessor;
use App\Services\BinService;
use App\Services\RateService;

class TransactionProcessorTest extends TestCase {
  public function testProcessTransactions(): void {
    $binServiceMock = $this->createMock(BinService::class);
    $rateServiceMock = $this->createMock(RateService::class);
    $binServiceMock->method('getBinDetails')
      ->willReturnMap([
        ['45717360', (object)['isEu' => true]],
        ['516793', (object)['isEu' => false]],
        ['45417360', (object)['isEu' => false]],
        ['41417360', (object)['isEu' => false]],
        ['4745030', (object)['isEu' => false]]
      ]);

    $rateServiceMock->method('getRate')
      ->willReturnMap([
        ['EUR', 1.0],
        ['USD', 1.1],
        ['JPY', 110],
        ['GBP', 0.9]
      ]);

    $processor = new TransactionProcessor($binServiceMock, $rateServiceMock);
    $results = $processor->processTransactions(__DIR__ . '/fixtures/input.json');

    $expectedResults = [1.0, 0.91, 0.04, 2.73, 44.44];
    $this->assertEquals($expectedResults, $results);
  }
}
