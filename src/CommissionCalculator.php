<?php

namespace App;

class CommissionCalculator {
  public static function calculate(float $amount, float $rate, bool $isEu): float {
    return ($amount / $rate) * ($isEu ? 0.01 : 0.02);
  }
}
