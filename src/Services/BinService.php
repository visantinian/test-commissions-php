<?php

namespace App\Services;

use GuzzleHttp\ClientInterface;

class BinService {
  private ClientInterface $client;

  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  public function getBinDetails(string $bin): object {
    $response = $this->client->request('GET', "https://lookup.binlist.net/$bin");
    $data = json_decode($response->getBody()->getContents());
    return (object) ['isEu' => $this->isEu($data->country->alpha2)];
  }

  private function isEu(string $countryCode): bool {
    return in_array($countryCode, [
      'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI',
      'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT',
      'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK'
    ]);
  }
}
