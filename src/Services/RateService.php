<?php

namespace App\Services;

use GuzzleHttp\ClientInterface;

class RateService {
  private ClientInterface $client;

  public function __construct(ClientInterface $client) {
    $this->client = $client;
  }

  public function getRate(string $currency): float {
    $response = $this->client->request('GET', 'https://api.exchangeratesapi.io/latest');
    $data = json_decode($response->getBody()->getContents(), true);
    return $data['rates'][$currency] ?? 0.0;
  }
}
