<?php
namespace App;


use App\Services\BinService;
use App\Services\RateService;

class TransactionProcessor {
  private BinService $binService;
  private RateService $rateService;

  public function __construct(BinService $binService, RateService $rateService) {
    $this->binService = $binService;
    $this->rateService = $rateService;
  }

  public function processTransactions(string $filePath): array {
    $transactions = json_decode(file_get_contents($filePath));
    $results = [];

    foreach ($transactions as $transaction) {
      $binDetails = $this->binService->getBinDetails($transaction->bin);
      $rate = $this->rateService->getRate($transaction->currency);
      $commission = CommissionCalculator::calculate(
        $transaction->amount,
        $rate,
        $binDetails->isEu
      );
      $results[] = round($commission, 2); // ceiling to the next cent
    }

    return $results;
  }
}
